package com.realtouchapp.mycamera2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, Camera2Activity.class));
            }
        });

    }

    private void stringToAsciiBytes(String password) {
        int[] asciiBytes = new int[ password.length() ];
        for (int i = 0; i < password.length(); i ++) {
            char c = password.charAt(i);
            asciiBytes[i] = (int)c;
        }

        Log.e("password", "stringToAsciiBytes: password = " + password);
        for (int b : asciiBytes) {
            Log.e("password", "stringToAsciiBytes: " + String.format("0x%02x", b));
        }

    }

}
