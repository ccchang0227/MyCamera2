package com.realtouchapp.mycamera2;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.File;

public class StillImageActivity extends AppCompatActivity {
    public final static String IMAGE_FILE_URL = "StillImageActivity.IMAGE_FILE_URL";

    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_still_image);

        imageView = findViewById(R.id.imageView);

        String fileUrl = getIntent().getStringExtra(IMAGE_FILE_URL);
        if (fileUrl != null) {
            File file = new File(fileUrl);
            if (file.exists()) {
                Glide.with(this)
                        .load(file)
                        .fitCenter()
                        .into(imageView);

//                Bitmap myBitmap = BitmapFactory.decodeFile(file.getAbsolutePath());
//                imageView.setImageBitmap(myBitmap);
            }
        }

    }
}
