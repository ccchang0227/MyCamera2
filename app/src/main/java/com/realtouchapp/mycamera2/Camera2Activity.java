package com.realtouchapp.mycamera2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.ImageFormat;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CameraMetadata;
import android.hardware.camera2.CaptureFailure;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.CaptureResult;
import android.hardware.camera2.TotalCaptureResult;
import android.hardware.camera2.params.MeteringRectangle;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.util.Log;
import android.util.Range;
import android.util.Rational;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MotionEvent;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.Toast;

import com.realtouchapp.plugin.AutoFitTextureView;
import com.realtouchapp.plugin.RuntimePermissionActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

public class Camera2Activity extends AppCompatActivity {
    private static final String TAG = "Camera2Activity";

    private static final SparseIntArray ORIENTATION = new SparseIntArray();
    static {
        ORIENTATION.append(Surface.ROTATION_0, 90);
        ORIENTATION.append(Surface.ROTATION_90, 0);
        ORIENTATION.append(Surface.ROTATION_180, 270);
        ORIENTATION.append(Surface.ROTATION_270, 180);
    }

    private AutoFitTextureView textureView;
    private RelativeLayout rl_content;
    private Button btn_af_mode;
    private SeekBar sb_ev;

    private String[] contextMenuItems;
    private int contextMenuType;

    private Range<Float> evRange = null;
    private Rational evStep = null;

    @SuppressWarnings("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //全屏无状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_camera2);

        textureView = findViewById(R.id.textureView);
        rl_content = findViewById(R.id.rl_content);
        rl_content.setClickable(true);
        rl_content.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() != MotionEvent.ACTION_UP) {
                    return false;
                }

                float x = motionEvent.getX();
                float y = motionEvent.getY();

                Log.e(TAG, "onTouch: x = " + x + ", y = " + y);

                autoFocusAtPoint(x, y);

                return false;
            }
        });

        btn_af_mode = findViewById(R.id.btn_af_mode);
        btn_af_mode.setVisibility(View.GONE);
        registerForContextMenu(btn_af_mode);
        btn_af_mode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mCameraCaptureSession == null) {
                    return;
                }
                if (mCaptureRequestBuilder == null) {
                    return;
                }

                CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
                try {
                    CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraId);
                    int[] availableAfModes = characteristics.get(CameraCharacteristics.CONTROL_AF_AVAILABLE_MODES);
                    if (availableAfModes != null) {
                        ArrayList<String> afModes = new ArrayList<>();
                        for (int i : availableAfModes) {
                            Log.e(TAG, "onClick: availableAfModes = " + i);
                            switch (i) {
                                case CameraCharacteristics.CONTROL_AF_MODE_OFF:
                                    afModes.add("Off");
                                    break;
                                case CameraCharacteristics.CONTROL_AF_MODE_AUTO:
                                    afModes.add("Auto");
                                    break;
                                case CameraCharacteristics.CONTROL_AF_MODE_CONTINUOUS_PICTURE:
                                    afModes.add("Continuous picture");
                                    break;
                                case CameraCharacteristics.CONTROL_AF_MODE_MACRO:
                                    afModes.add("Macro");
                                    break;
                                case CameraCharacteristics.CONTROL_AF_MODE_CONTINUOUS_VIDEO:
                                    afModes.add("Continuous video");
                                    break;
                                case CameraCharacteristics.CONTROL_AF_MODE_EDOF:
                                    afModes.add("Extended depth of field");
                                    break;
                            }
                        }

                        contextMenuItems = afModes.toArray(new String[afModes.size()]);
                        openContextMenu(view);
                    }

                } catch (CameraAccessException e) {
                    e.printStackTrace();
                } catch (Throwable e) {
                    e.printStackTrace();
                }

            }
        });

        sb_ev = findViewById(R.id.sb_ev);
        sb_ev.setMax(2);
        sb_ev.setProgress(1);
        sb_ev.setVisibility(View.GONE);
        sb_ev.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (!b) {
                    return;
                }
                if (evRange == null || evStep == null) {
                    seekBar.setVisibility(View.GONE);
                    return;
                }

                int max = seekBar.getMax() / 2;
                int min = -seekBar.getMax() / 2;
                int progress = i + min;
                Log.e(TAG, "onProgressChanged: " + progress + ", max = " + max + ", min = " + min);
                setEvValue(progress);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                if (evRange == null || evStep == null) {
                    seekBar.setVisibility(View.GONE);
                }
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        findViewById(R.id.btn_take_picture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takePicture(view);
            }
        });

        if (Build.VERSION.SDK_INT >= 23) {
            requestForPermission();
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        for (int i = 0; i < contextMenuItems.length; i ++) {
            menu.add(Menu.NONE, Menu.FIRST+i, Menu.NONE, contextMenuItems[i]);
        }
        menu.add(Menu.NONE, Menu.FIRST+contextMenuItems.length, Menu.NONE, "Cancel");

        super.onCreateContextMenu(menu, v, menuInfo);
    }

    @Override
    protected void onResume() {
        super.onResume();
        startCameraThread();
        if (!textureView.isAvailable()) {
            textureView.setSurfaceTextureListener(getSurfaceTextureListener());
        } else {
            openCamera();
//            startPreview();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (mCameraCaptureSession != null) {
            mCameraCaptureSession.close();
            mCameraCaptureSession = null;
        }

        if (mCameraDevice != null) {
            mCameraDevice.close();
            mCameraDevice = null;
        }

        if (mPreviewReader != null) {
            mPreviewReader.close();
            mPreviewReader = null;
        }

    }

    @Override
    protected void onDestroy() {

        if (mImageReader != null) {
            mImageReader.close();
            mImageReader = null;
        }

        mCameraThread.quitSafely();
        try {
            mCameraThread.join();
            mCameraThread = null;
            mCameraHandler = null;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        unregisterForContextMenu(btn_af_mode);

        super.onDestroy();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (Activity.RESULT_OK != resultCode) return;
        if (REQUEST_STORAGE == requestCode && data != null) {
            final boolean isGranted = data.getBooleanExtra(RuntimePermissionActivity.REQUESTED_PERMISSION, false);
            if (isGranted) {

            }
            else{
                finish();
            }
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    // MARK: -

    private static final int REQUEST_STORAGE = 1;
    private void requestForPermission() {
        RuntimePermissionActivity.startActivity(Camera2Activity.this,
                REQUEST_STORAGE,
                Manifest.permission.CAMERA,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    private void startCameraThread() {
        mCameraThread = new HandlerThread("CameraThread");
        mCameraThread.start();
        mCameraHandler = new Handler(mCameraThread.getLooper());
    }

    private TextureView.SurfaceTextureListener getSurfaceTextureListener() {
        return new TextureView.SurfaceTextureListener() {
            @Override
            public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
                Log.e(TAG, "onSurfaceTextureAvailable: i = " + i + ", i1 = " + i1);
                //当SurefaceTexture可用的时候，设置相机参数并打开相机
                setupCamera(i, i1);
                openCamera();
            }

            @Override
            public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
                Log.e(TAG, "onSurfaceTextureSizeChanged: i = " + i + ", i1 = " + i1);
            }

            @Override
            public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
                Log.e(TAG, "onSurfaceTextureDestroyed:");

                return false;
            }

            @Override
            public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

            }
        };
    }

    private String mCameraId;
    private Size mPreviewSize;
    private Size mCaptureSize;
    private HandlerThread mCameraThread;
    private Handler mCameraHandler;
    private CameraDevice mCameraDevice;
    private ImageReader mImageReader;
    private ImageReader mPreviewReader;
    private CaptureRequest.Builder mCaptureRequestBuilder;
    private CaptureRequest mCaptureRequest;
    private CameraCaptureSession mCameraCaptureSession;

    private void setupCamera(int width, int height) {
        // 默认打开前置摄像头
        int preferredFacing = CameraCharacteristics.LENS_FACING_FRONT;

        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {

            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);

                Integer orientation = characteristics.get(CameraCharacteristics.SENSOR_ORIENTATION);
                Log.e(TAG, "setupCamera: facing = " + facing + ", orientation = " + orientation);
            }

            //遍历所有摄像头
            for (String cameraId : manager.getCameraIdList()) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
                Integer facing = characteristics.get(CameraCharacteristics.LENS_FACING);
                if (facing != null && facing != preferredFacing) {
                    continue;
                }

                //获取StreamConfigurationMap，它是管理摄像头支持的所有输出格式和尺寸
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                assert map != null;
                //根据TextureView的尺寸设置预览尺寸
                mPreviewSize = getOptimalSize(map.getOutputSizes(SurfaceTexture.class), width, height);
                //获取相机支持的最大拍照尺寸
                mCaptureSize = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new Comparator<Size>() {
                    @Override
                    public int compare(Size lhs, Size rhs) {
                        return Long.signum(lhs.getWidth() * lhs.getHeight() - rhs.getHeight() * rhs.getWidth());
                    }
                });

                //此ImageReader用于拍照所需
                setupImageReader();
                mCameraId = cameraId;
                break;
            }
            if (mCameraId == null && manager.getCameraIdList().length > 0) {
                mCameraId = manager.getCameraIdList()[0];

                CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraId);

                //获取StreamConfigurationMap，它是管理摄像头支持的所有输出格式和尺寸
                StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
                assert map != null;
                //根据TextureView的尺寸设置预览尺寸
                mPreviewSize = getOptimalSize(map.getOutputSizes(SurfaceTexture.class), width, height);
                //获取相机支持的最大拍照尺寸
                Size[] pictureSizes = map.getOutputSizes(ImageFormat.JPEG);
                for (Size size : pictureSizes) {
                    if (size.getWidth() == 3264 && size.getHeight() == 2448) {
                        mCaptureSize = size;
                        break;
                    }
                }
//                mCaptureSize = Collections.max(Arrays.asList(map.getOutputSizes(ImageFormat.JPEG)), new Comparator<Size>() {
//                    @Override
//                    public int compare(Size lhs, Size rhs) {
//                        return Long.signum(lhs.getWidth() * lhs.getHeight() - rhs.getHeight() * rhs.getWidth());
//                    }
//                });

                //此ImageReader用于拍照所需
                setupImageReader();
            }

            Log.e(TAG, "setupCamera: cameraId = " + mCameraId);
            Log.e(TAG, "setupCamera: previewSize = " + mPreviewSize.toString());
            Log.e(TAG, "setupCamera: captureSize = " + mCaptureSize.toString());

        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        configureTransform(width, height);
    }

    //选择sizeMap中大于并且最接近width和height的size
    private Size getOptimalSize(Size[] sizeMap, int width, int height) {
        List<Size> sizeList = new ArrayList<>();
        for (Size option : sizeMap) {
            if (width > height) {
                if (option.getWidth() > width && option.getHeight() > height) {
                    sizeList.add(option);
                }
            } else {
                if (option.getWidth() > height && option.getHeight() > width) {
                    sizeList.add(option);
                }
            }
        }
        if (sizeList.size() > 0) {
            return Collections.min(sizeList, new Comparator<Size>() {
                @Override
                public int compare(Size lhs, Size rhs) {
                    return Long.signum(lhs.getWidth() * lhs.getHeight() - rhs.getWidth() * rhs.getHeight());
                }
            });
        }
        return sizeMap[0];
    }

    private void configureTransform(int width, int height) {
        Activity activity = this;
        if (textureView == null || mPreviewSize == null) {
            return;
        }

        int orientation = getResources().getConfiguration().orientation;
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        Log.e(TAG, "configureTransform: rotation = " + rotation);
        Log.e(TAG, "configureTransform: orientation = " + orientation);

        Matrix matrix = new Matrix();
        RectF viewRect = new RectF(0, 0, Math.min(width, height), Math.max(width, height));
        RectF bufferRect = new RectF(0, 0, Math.min(mPreviewSize.getWidth(), mPreviewSize.getHeight()), Math.max(mPreviewSize.getWidth(), mPreviewSize.getHeight()));
        if (orientation == Configuration.ORIENTATION_LANDSCAPE) {
            // In landscape
            Log.e(TAG, "configureTransform: In landscape");
//            viewRect.right = Math.max(width, height);
//            viewRect.bottom = Math.min(width, height);
//            bufferRect.right = Math.max(mPreviewSize.getWidth(), mPreviewSize.getHeight());
//            bufferRect.bottom = Math.min(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        } else {
            // In portrait
            Log.e(TAG, "configureTransform: In portrait");
        }

        float centerX = viewRect.centerX();
        float centerY = viewRect.centerY();
        if (Surface.ROTATION_90 == rotation || Surface.ROTATION_270 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());

            matrix.postScale((float) mPreviewSize.getWidth() / width, (float) mPreviewSize.getHeight() / height, centerX, centerY);

            // FIXME: min -> max when using FILL
            float scale = Math.min(viewRect.width() / bufferRect.width(), viewRect.height() / bufferRect.height());

            float scaleX = viewRect.height() / bufferRect.height();
            float scaleY = viewRect.width() / bufferRect.width();
            scaleX = scaleY = scale;

            matrix.postScale(scaleX, scaleY, centerX, centerY);

//            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.CENTER);
//            matrix.postScale((float) height / mPreviewSize.getHeight(), (float) width / mPreviewSize.getWidth(), centerX, centerY);

            matrix.postRotate(90 * (rotation - 2), centerX, centerY);
//            matrix.postScale(-1, 1, centerX, centerY);

//            matrix.postScale(0.5f, 0.5f, centerX, centerY);

        } else if (Surface.ROTATION_180 == rotation) {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());
            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.CENTER);
            matrix.postScale((float) width / mPreviewSize.getWidth(), (float) height / mPreviewSize.getHeight(), centerX, centerY);

            matrix.postRotate(180, centerX, centerY);
        }
        else {
            bufferRect.offset(centerX - bufferRect.centerX(), centerY - bufferRect.centerY());

            float scale = Math.max(viewRect.width() / bufferRect.width(), viewRect.height() / bufferRect.height());

            float scaleX = viewRect.height() / bufferRect.height();
            float scaleY = viewRect.width() / bufferRect.width();
            scaleX = scaleY = scale;

            matrix.postScale(scaleX, scaleY, centerX, centerY);

//            matrix.setRectToRect(viewRect, bufferRect, Matrix.ScaleToFit.CENTER);
//            matrix.postScale((float) width / mPreviewSize.getWidth(), (float) height / mPreviewSize.getHeight(), centerX, centerY);
        }

//        matrix.postScale(0.5f, 0.5f, centerX, centerY);

        textureView.setTransform(matrix);
    }

    private void setupPreviewReader() {
        //前三个参数分别是需要的尺寸和格式，最后一个参数代表每次最多获取几帧数据，本例的2代表ImageReader中最多可以获取两帧图像流
        mPreviewReader = ImageReader.newInstance(mPreviewSize.getWidth(), mPreviewSize.getHeight(),
                ImageFormat.JPEG, 2);
        //监听ImageReader的事件，当有图像流数据可用时会回调onImageAvailable方法，它的参数就是预览帧数据，可以对这帧数据进行处理
        mPreviewReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {
                Image image = reader.acquireLatestImage();
                //我们可以将这帧数据转成字节数组，类似于Camera1的PreviewCallback回调的预览帧数据
                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] data = new byte[buffer.remaining()];
                buffer.get(data);
                image.close();
            }
        }, null);
    }

    private void openCamera() {
        //检查权限
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        //获取摄像头的管理者CameraManager
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            //打开相机，第一个参数指示打开哪个摄像头，第二个参数stateCallback为相机的状态回调接口，第三个参数用来确定Callback在哪个线程执行，为null的话就在当前线程执行
            if (manager != null) {
                manager.openCamera(mCameraId, stateCallback, mCameraHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private final CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(CameraDevice camera) {
            mCameraDevice = camera;
            Log.e(TAG, "onOpened: camera = " + camera);

            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    fetchExposureCompensationValues();
                }
            });

            //开启预览
            startPreview();
        }

        @Override
        public void onDisconnected(CameraDevice cameraDevice) {
            Log.e(TAG, "onDisconnected");
            cameraDevice.close();
            if (cameraDevice == mCameraDevice) {
                mCameraDevice = null;
            }
        }

        @Override
        public void onError(CameraDevice cameraDevice, int i) {
            Log.e(TAG, "onError: " + i);
            cameraDevice.close();
            if (cameraDevice == mCameraDevice) {
                mCameraDevice = null;
            }
        }
    };

    private void startPreview() {
        SurfaceTexture mSurfaceTexture = textureView.getSurfaceTexture();
        //设置TextureView的缓冲区大小
        mSurfaceTexture.setDefaultBufferSize(mPreviewSize.getWidth(), mPreviewSize.getHeight());
        //获取Surface显示预览数据
        Surface mSurface = new Surface(mSurfaceTexture);
        try {
            //创建CaptureRequestBuilder，TEMPLATE_PREVIEW比表示预览请求
            mCaptureRequestBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            //设置Surface作为预览数据的显示界面
            mCaptureRequestBuilder.addTarget(mSurface);

            try {
                //設置自動對焦
                mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_CONTINUOUS_PICTURE);
                mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
            } catch (Throwable e) {
                e.printStackTrace();
            }

            //创建相机捕获会话，第一个参数是捕获数据的输出Surface列表，第二个参数是CameraCaptureSession的状态回调接口，当它创建好后会回调onConfigured方法，第三个参数用来确定Callback在哪个线程执行，为null的话就在当前线程执行
            mCameraDevice.createCaptureSession(Arrays.asList(mSurface, mImageReader.getSurface()), new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(CameraCaptureSession session) {
                    try {
                        //创建捕获请求
                        mCaptureRequest = mCaptureRequestBuilder.build();
                        mCameraCaptureSession = session;
                        //设置反复捕获数据的请求，这样预览界面就会一直有数据显示
                        mCameraCaptureSession.setRepeatingRequest(mCaptureRequest, new CameraCaptureSession.CaptureCallback() {
                            @Override
                            public void onCaptureCompleted(@NonNull CameraCaptureSession session, @NonNull CaptureRequest request, @NonNull TotalCaptureResult result) {
                                super.onCaptureCompleted(session, request, result);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        autoFocusAtPoint(rl_content.getWidth() / 2f, rl_content.getHeight() / 2f);
                                    }
                                });


                            }

                        }, mCameraHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onConfigureFailed(CameraCaptureSession session) {

                }
            }, mCameraHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    public void takePicture(View view) {
//        lockFocus();
        capture();
    }

    private void lockFocus() {
        try {
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
            mCameraCaptureSession.capture(mCaptureRequestBuilder.build(), mCaptureCallback, mCameraHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private CameraCaptureSession.CaptureCallback mCaptureCallback = new CameraCaptureSession.CaptureCallback() {
        @Override
        public void onCaptureProgressed(CameraCaptureSession session, CaptureRequest request, CaptureResult partialResult) {
        }

        @Override
        public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
            capture();
        }
    };

    private int getJpegOrientation(CameraCharacteristics c, int deviceOrientation) {
        if (deviceOrientation == android.view.OrientationEventListener.ORIENTATION_UNKNOWN) return 0;
        int sensorOrientation = c.get(CameraCharacteristics.SENSOR_ORIENTATION);

        int surfaceRotation;
        switch (deviceOrientation) {
            case Surface.ROTATION_90:
                surfaceRotation = 90;
                break;
            case Surface.ROTATION_180:
                surfaceRotation = 180;
                break;
            case Surface.ROTATION_270:
                surfaceRotation = 270;
                break;
            default:
                surfaceRotation = 0;
                break;
        }
//        int surfaceRotation = ORIENTATION.get(deviceOrientation);

        // Round device orientation to a multiple of 90
        surfaceRotation = (surfaceRotation + 45) / 90 * 90;

        // Reverse device orientation for front-facing cameras
        boolean facingFront = c.get(CameraCharacteristics.LENS_FACING) == CameraCharacteristics.LENS_FACING_FRONT;
        if (!facingFront) surfaceRotation = -surfaceRotation;

        // Calculate desired JPEG orientation relative to camera orientation to make
        // the image upright relative to the device orientation
        int jpegOrientation = (sensorOrientation + surfaceRotation + 360) % 360;

        return jpegOrientation;
    }

    private long captureStartTimeInterval;
    private ProgressDialog progressDialog;

    private void capture() {
        try {
            final CaptureRequest.Builder mCaptureBuilder = mCameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_STILL_CAPTURE);
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            mCaptureBuilder.addTarget(mImageReader.getSurface());
//            mCaptureBuilder.set(CaptureRequest.JPEG_ORIENTATION, ORIENTATION.get(rotation));

            CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
            try {
                if (manager != null) {
                    CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraId);
                    mCaptureBuilder.set(CaptureRequest.JPEG_ORIENTATION, getJpegOrientation(characteristics, rotation));
                }
            } catch (CameraAccessException e) {
                e.printStackTrace();
            } catch (NullPointerException e) {
                e.printStackTrace();
            }

            if (sb_ev.getVisibility() == View.VISIBLE) {
                int min = -sb_ev.getMax() / 2;
                int progress = sb_ev.getProgress() + min;
                mCaptureBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, progress);
            }

            CameraCaptureSession.CaptureCallback CaptureCallback = new CameraCaptureSession.CaptureCallback() {
                @Override
                public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
//                    Toast.makeText(getApplicationContext(), "Image Saved!", Toast.LENGTH_LONG).show();
//                    unLockFocus();

                    try {
                        mCameraCaptureSession.setRepeatingRequest(mCaptureRequest, null, mCameraHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }

                }

                @Override
                public void onCaptureStarted( CameraCaptureSession session, CaptureRequest request, long timestamp, long frameNumber) {
                    super.onCaptureStarted(session, request, timestamp, frameNumber);

                    captureStartTimeInterval = System.currentTimeMillis();

                    progressDialog = ProgressDialog.show(Camera2Activity.this, "拍照中...", null, true);

                }
            };
            mCameraCaptureSession.stopRepeating();
            mCameraCaptureSession.capture(mCaptureBuilder.build(), CaptureCallback, mCameraHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void unLockFocus() {
        try {
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
            //mCameraCaptureSession.capture(mCaptureRequestBuilder.build(), null, mCameraHandler);
            mCameraCaptureSession.setRepeatingRequest(mCaptureRequest, null, mCameraHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    private void setupImageReader() {
        //2代表ImageReader中最多可以获取两帧图像流
        mImageReader = ImageReader.newInstance(mCaptureSize.getWidth(), mCaptureSize.getHeight(),
                ImageFormat.JPEG, 2);
        mImageReader.setOnImageAvailableListener(new ImageReader.OnImageAvailableListener() {
            @Override
            public void onImageAvailable(ImageReader reader) {

                mCameraHandler.post(new ImageProcessor(reader.acquireNextImage(), Camera2Activity.this));
//                mCameraHandler.post(new imageSaver(Camera2Activity.this, reader.acquireNextImage()));

            }
        }, mCameraHandler);
    }

    public static class imageSaver implements Runnable {

        WeakReference<Camera2Activity> weakActivity;

        private Image mImage;

        public imageSaver(Camera2Activity activity, Image image) {
            weakActivity = new WeakReference<>(activity);
            mImage = image;
        }

        @Override
        public void run() {
            ByteBuffer buffer = mImage.getPlanes()[0].getBuffer();
            byte[] data = new byte[buffer.remaining()];
            buffer.get(data);
            String path = Environment.getExternalStorageDirectory() + "/DCIM/CameraV2/";
            File mImageFile = new File(path);
            if (!mImageFile.exists()) {
                mImageFile.mkdir();
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String fileName = path + "IMG_" + timeStamp + ".jpg";
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(fileName);
                fos.write(data, 0, data.length);
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (fos != null) {
                    try {
                        fos.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    final String filePath = fileName;
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            Camera2Activity activity = weakActivity.get();
                            if (activity != null) {
                                Intent intent = new Intent(activity, StillImageActivity.class);
                                intent.putExtra(StillImageActivity.IMAGE_FILE_URL, filePath);
                                activity.startActivity(intent);
                            }

                        }
                    });

                }
            }
        }
    }

    private void fetchExposureCompensationValues() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        CameraCharacteristics characteristics = null;
        try {
            if (manager != null) {
                characteristics = manager.getCameraCharacteristics(mCameraId);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();

            evRange = null;
            evStep = null;
            sb_ev.setVisibility(View.GONE);
            return;
        }
        if (characteristics == null) {
            evRange = null;
            evStep = null;
            sb_ev.setVisibility(View.GONE);
            return;
        }

        Range<Integer> range = characteristics.get(CameraCharacteristics.CONTROL_AE_COMPENSATION_RANGE);
        if (range != null && range.getUpper() != 0 && range.getLower() != 0) {
            Rational step = characteristics.get(CameraCharacteristics.CONTROL_AE_COMPENSATION_STEP);
            if (step != null && step.isFinite() && !step.isNaN()) {
                evStep = step;
                Log.e(TAG, "Exposure compensation step = " + step.getNumerator() + "/" + step.getDenominator());
                Log.e(TAG, "Exposure compensation =  " + step.floatValue());

                float min = range.getLower() * step.floatValue();
                float max = range.getUpper() * step.floatValue();
                Log.e(TAG, "Exposure compensation range from " + min + " to " + max);
                evRange = new Range<>(min, max);

                int seekBarRange = (int) ((max - min) / step.floatValue());
                sb_ev.setMax(seekBarRange);
                sb_ev.setProgress(seekBarRange / 2);
                sb_ev.setVisibility(View.VISIBLE);

            }
            else {
                Log.e(TAG, "Exposure compensation step is null.");

                evRange = null;
                evStep = null;
                sb_ev.setVisibility(View.GONE);
            }
        }
        else {
            Log.e(TAG, "Exposure compensation is not supported.");

            evRange = null;
            evStep = null;
            sb_ev.setVisibility(View.GONE);
        }

    }

    private void setEvValue(int ev) {
        if (evRange == null || evStep == null) {
            // EV control is not supported.
            return;
        }

        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        CameraCharacteristics characteristics = null;
        try {
            if (manager != null) {
                characteristics = manager.getCameraCharacteristics(mCameraId);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();

            return;
        }
        if (characteristics == null) {
            return;
        }

        CameraCaptureSession.CaptureCallback captureCallbackHandler = new CameraCaptureSession.CaptureCallback() {
            @Override
            public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                super.onCaptureCompleted(session, request, result);

                if (request.getTag() == "EV_CONTROL") {
                    Log.e(TAG, "onCaptureCompleted: Set Exposure compensation complete");

                    try {
                        mCameraCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, mCameraHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCaptureFailed(CameraCaptureSession session, CaptureRequest request, CaptureFailure failure) {
                super.onCaptureFailed(session, request, failure);
                Log.e(TAG, "Set Exposure compensation failure: " + failure);
            }
        };

        //first stop the existing repeating request
        try {
            mCameraCaptureSession.stopRepeating();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, ev);
        mCaptureRequestBuilder.setTag("EV_CONTROL");
        try {
            mCameraCaptureSession.capture(mCaptureRequestBuilder.build(), captureCallbackHandler, mCameraHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

    }

    private void autoFocusAtPoint(float x, float y) {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        CameraCharacteristics characteristics = null;
        try {
            if (manager != null) {
                characteristics = manager.getCameraCharacteristics(mCameraId);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();

            return;
        }
        if (characteristics == null) {
            return;
        }

        MeteringRectangle rectangle = calcTapAreaForCamera2(characteristics, 150, MeteringRectangle.METERING_WEIGHT_MAX - 1, x, y);

        CameraCaptureSession.CaptureCallback captureCallbackHandler = new CameraCaptureSession.CaptureCallback() {
            @Override
            public void onCaptureCompleted(CameraCaptureSession session, CaptureRequest request, TotalCaptureResult result) {
                super.onCaptureCompleted(session, request, result);
//                mManualFocusEngaged = false;

                if (request.getTag() == "FOCUS_TAG") {
                    Log.e(TAG, "Manual AF complete");

                    //the focus trigger is complete -
                    //resume repeating (preview surface will get frames), clear AF trigger
                    mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CaptureRequest.CONTROL_AF_TRIGGER_IDLE);
                    mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_IDLE);
                    mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
                    mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
//                    try {
//                        mCameraCaptureSession.capture(mCaptureRequestBuilder.build(), null, mCameraHandler);
//                    } catch (CameraAccessException e) {
//                        e.printStackTrace();
//                    }
                    try {
                        mCameraCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, mCameraHandler);
                    } catch (CameraAccessException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCaptureFailed(CameraCaptureSession session, CaptureRequest request, CaptureFailure failure) {
                super.onCaptureFailed(session, request, failure);
                Log.e(TAG, "Manual AF failure: " + failure);
//                mManualFocusEngaged = false;
            }
        };

        //first stop the existing repeating request
        try {
            mCameraCaptureSession.stopRepeating();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }

        //cancel any existing AF trigger (repeated touches, etc.)
//        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_CANCEL);
//        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_OFF);
//        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_OFF);
//        try {
//            mCameraCaptureSession.capture(mCaptureRequestBuilder.build(), captureCallbackHandler, mCameraHandler);
//        } catch (CameraAccessException e) {
//            e.printStackTrace();
//        }

        //Now add a new AF trigger with focus region
        if (isMeteringAreaAFSupported()) {
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_REGIONS, new MeteringRectangle[]{rectangle});
        }
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_MODE, CameraMetadata.CONTROL_MODE_AUTO);
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE, CaptureRequest.CONTROL_AF_MODE_AUTO);
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER, CameraMetadata.CONTROL_AF_TRIGGER_START);
        if (isMeteringAreaAESupported()) {
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_REGIONS, new MeteringRectangle[]{rectangle});
        }
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_MODE, CaptureRequest.CONTROL_AE_MODE_ON);
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER, CaptureRequest.CONTROL_AE_PRECAPTURE_TRIGGER_START);
        mCaptureRequestBuilder.setTag("FOCUS_TAG"); //we'll capture this later for resuming the preview

        if (sb_ev.getVisibility() == View.VISIBLE) {
            mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_EXPOSURE_COMPENSATION, 0);
            sb_ev.setProgress(sb_ev.getMax() / 2);
        }

        //then we ask for a single request (not repeating!)
        try {
            mCameraCaptureSession.capture(mCaptureRequestBuilder.build(), captureCallbackHandler, mCameraHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
//        mManualFocusEngaged = true;

    }

    private boolean isMeteringAreaAFSupported() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (manager != null) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraId);
                return characteristics.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AF) >= 1;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return false;
    }

    private boolean isMeteringAreaAESupported() {
        CameraManager manager = (CameraManager) getSystemService(Context.CAMERA_SERVICE);
        try {
            if (manager != null) {
                CameraCharacteristics characteristics = manager.getCameraCharacteristics(mCameraId);
                return characteristics.get(CameraCharacteristics.CONTROL_MAX_REGIONS_AE) >= 1;
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        } catch (NullPointerException e) {
            e.printStackTrace();
        }

        return false;
    }

    private MeteringRectangle calcTapAreaForCamera2(CameraCharacteristics c, int areaSize, int weight, float currentX, float currentY) {
        // 获取Size
        Rect rect = c.get(CameraCharacteristics.SENSOR_INFO_ACTIVE_ARRAY_SIZE);
        Log.e(TAG, "active Rect:" + rect.toString());

        float scale = rl_content.getWidth() / (float)mPreviewSize.getHeight();
        if (scale * mPreviewSize.getWidth() > rl_content.getHeight()) {
            scale = rl_content.getHeight() / (float)mPreviewSize.getWidth();
        }
        float previewWidth = mPreviewSize.getHeight() * scale;
        float previewHeight = mPreviewSize.getWidth() * scale;

        // FIXME: change comment when using FILL
        float newX, newY;
        newX = currentX;
        newY = currentY;
//        switch (this.scaleType) {
//            case AspectFit: {
                newX = currentX - (rl_content.getWidth() - previewWidth) / 2.0f;
                newY = currentY - (rl_content.getHeight() - previewHeight) / 2.0f;
//                break;
//            }
//            case AspectFill: {
//                scale = rl_content.getWidth() / (float)mPreviewSize.getHeight();
//                if (scale * mPreviewSize.getWidth() < rl_content.getHeight()) {
//                    scale = rl_content.getHeight() / (float)mPreviewSize.getWidth();
//                }
//                previewWidth = mPreviewSize.getHeight() * scale;
//                previewHeight = mPreviewSize.getWidth() * scale;
//
//                newX = currentX - (rl_content.getWidth() - previewWidth) / 2.0f;
//                newY = currentY - (rl_content.getHeight() - previewHeight) / 2.0f;
//
//                break;
//            }
//        }
        if (newX < 0 || newY < 0 || newX > previewWidth || newY > previewHeight) {
            newX = previewWidth / 2.0f;
            newY = previewHeight / 2.0f;
        }

        float newNewY = newX;
        float newNewX = newY;

        Rect newRect;
        int leftPos, topPos;
        // 大小转换
        leftPos = (int) ((newNewX / previewHeight) * rect.width());
        topPos = (int) ((newNewY / previewWidth) * rect.height());
        // 以坐标点为中心生成一个矩形, 需要防止上下左右的值溢出
        int left = clamp(leftPos - areaSize, 0, rect.right);
        int top = clamp(topPos - areaSize, 0, rect.bottom);
        int right = clamp(leftPos + areaSize, leftPos, rect.right);
        int bottom = clamp(topPos + areaSize, topPos, rect.bottom);
        newRect = new Rect(left, top, right, bottom);
        Log.e(TAG, newRect.toString());
        // 构造MeteringRectangle

//        return new MeteringRectangle(new Rect(0, 0, 300, 300), weight);
        return new MeteringRectangle(newRect, weight);
    }

    private int clamp(int x, int min, int max) {
        if (x > max) {
            return max;
        }
        if (x < min) {
            return min;
        }
        return x;
    }

    public void startControlAFRequest(MeteringRectangle rect,
                                      CameraCaptureSession.CaptureCallback captureCallback) {

        MeteringRectangle[] rectangle = new MeteringRectangle[]{rect};
        // 对焦模式必须设置为AUTO
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_MODE,CaptureRequest.CONTROL_AF_MODE_AUTO);
        //AE
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AE_REGIONS,rectangle);
        //AF 此处AF和AE用的同一个rect, 实际AE矩形面积比AF稍大, 这样测光效果更好
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_REGIONS,rectangle);
        try {
            // AE/AF区域设置通过setRepeatingRequest不断发请求
            mCameraCaptureSession.setRepeatingRequest(mCaptureRequestBuilder.build(), null, mCameraHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        //触发对焦
        mCaptureRequestBuilder.set(CaptureRequest.CONTROL_AF_TRIGGER,CaptureRequest.CONTROL_AF_TRIGGER_START);
        try {
            //触发对焦通过capture发送请求, 因为用户点击屏幕后只需触发一次对焦
            mCameraCaptureSession.capture(mCaptureRequestBuilder.build(), captureCallback, mCameraHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }


    public static class ImageProcessor implements Runnable {

        private Image image;
        private WeakReference<Camera2Activity> camera2ToolWeakReference;

        ImageProcessor(Image image, Camera2Activity camera2Tool) {
            this.image = image;
            this.camera2ToolWeakReference = new WeakReference<>(camera2Tool);
        }

        @Override
        public void run() {
            Bitmap bitmap = null;
            int bmWidth = 0;
            int bmHeight = 0;

            try {
                ByteBuffer buffer = image.getPlanes()[0].getBuffer();
                byte[] data = new byte[buffer.remaining()];
                buffer.get(data);

                bitmap = BitmapFactory.decodeByteArray(data , 0, data.length);
                bmWidth = bitmap.getWidth();
                bmHeight = bitmap.getHeight();
                Log.e(TAG, "onImageAvailable: " + bitmap.getWidth() + ", " + bitmap.getHeight());

            } catch (Throwable e) {
                e.printStackTrace();
            }
            image.close();

            final Camera2Activity tool = camera2ToolWeakReference.get();

            // FIXME: false -> true when using FILL
            Rect rect = null;
            Matrix matrix = null;
            if (false) {
                TextureView textureView = tool.textureView;
                float scale = bmWidth / (float)textureView.getWidth();
                if (scale * textureView.getHeight() > bmHeight) {
                    scale = bmHeight / (float)textureView.getHeight();
                }
                int viewWidth = (int) (textureView.getWidth() * scale);
                int viewHeight = (int) (textureView.getHeight() * scale);
                if (viewWidth % 2 != 0) {
                    viewWidth -= 1;
                }
                if (viewHeight % 2 != 0) {
                    viewHeight -= 1;
                }

                rect = new Rect();
                rect.left = (bmWidth - viewWidth) / 2;
                rect.top = (bmHeight - viewHeight) / 2;
                rect.right = rect.left + viewWidth;
                rect.bottom = rect.top + viewHeight;
            }

            if (false) {
                matrix = new Matrix();
                matrix.postScale(-1, 1);
            }

            if ((rect != null || matrix != null) && bitmap != null) {
                if (rect == null) {
                    rect = new Rect(0, 0, bmWidth, bmHeight);
                }

                try {
                    Bitmap newBitmap = Bitmap.createBitmap(bitmap, rect.left, rect.top, rect.width(), rect.height(), matrix, true);
                    if (newBitmap != bitmap) {
                        bitmap.recycle();
                        bitmap = newBitmap;
                    }

                } catch (Throwable e) {
                    e.printStackTrace();
                }
            }

            if (bitmap != null) {
                String path = Environment.getExternalStorageDirectory() + "/DCIM/CameraV2/";
                File mImageFile = new File(path);
                if (!mImageFile.exists()) {
                    mImageFile.mkdir();
                }
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
                String fileName = path + "IMG_" + timeStamp + ".jpg";
                FileOutputStream fos = null;
                try {
                    fos = new FileOutputStream(fileName);
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                } catch (Throwable e) {
                    e.printStackTrace();
                } finally {
                    if (fos != null) {
                        try {
                            fos.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }

                        bitmap.recycle();

                        final String filePath = fileName;
                        new Handler(Looper.getMainLooper()).post(new Runnable() {
                            @Override
                            public void run() {

                                long currentTimeInterval = System.currentTimeMillis();

                                Camera2Activity activity = tool;
                                if (activity != null) {
                                    long duration = currentTimeInterval - activity.captureStartTimeInterval;
                                    Toast.makeText(activity, "Image Saved. time used: " + String.format("%.2f", duration/1000.0f) + " sec.", Toast.LENGTH_SHORT).show();
                                    Log.e(TAG, "Image Saved. time used: " + String.format("%.2f", duration/1000.0f) + " sec.");

                                    if (activity.progressDialog != null) {
                                        activity.progressDialog.dismiss();
                                    }
                                    activity.progressDialog = null;

                                    Intent intent = new Intent(activity, StillImageActivity.class);
                                    intent.putExtra(StillImageActivity.IMAGE_FILE_URL, filePath);
                                    activity.startActivity(intent);
                                }

                            }
                        });

                    }
                }

            }

        }
    }

}
